'use strict'

/**
 * Завдання
 * Реалізувати функцію, яка дозволить оцінити, чи команда розробників встигне здати проект до настання дедлайну.
 * Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.
 *
 * Технічні вимоги:
 * Функція на вхід приймає три параметри:
 * масив із чисел, що становить швидкість роботи різних членів команди. Кількість елементів у масиві означає кількість людей у команді.
 * Кожен елемент означає скільки стор поінтів (умовна оцінка складності виконання завдання) може виконати даний розробник за 1 день.
 * масив з чисел, який є беклогом (список всіх завдань, які необхідно виконати). Кількість елементів у масиві означає кількість завдань у беклозі.
 * Кожне число в масиві означає кількість сторі поінтів, необхідні виконання даного завдання.
 * Дата дедлайну (об'єкт типу Date).
 * Після виконання, функція повинна порахувати, чи команда розробників встигне виконати всі завдання з беклогу до настання дедлайну
 * (робота ведеться починаючи з сьогоднішнього дня). Якщо так, вивести на екран повідомлення: Усі завдання будуть успішно виконані
 * за ? днів до настання дедлайну!. Підставити потрібну кількість днів у текст. Якщо ні, вивести повідомлення Команді
 * розробників доведеться витратити додатково ? годин після дедлайну, щоб виконати всі завдання в беклозі
 * Робота триває по 8 годин на день по будніх днях *
 */

let teamProductivity = [1, 2, 3, 4, 6];
let backLog = [10, 20, 100, 1150];
let deadLine = new Date(2022, 11, 31, 23, 59, 59);

console.log(`Team productivity: ${teamProductivity}`);
console.log(`Backlog: ${backLog}`);
console.log(`Deadline: ${deadLine}`);

function productivityEval(prod, tasks, deadline) {
    let dailyProductivity = prod.reduce((prev, curr) => {return prev + curr});
    let totalSkope = tasks.reduce((prev, curr) => {return prev + curr});

    function getWorkDaysTo(date) {

        let currDate = new Date();
        let workDays = 0;
        for (let i = 0; currDate <= deadline; i++) {
            (currDate.getDay() > 0 && currDate.getDay() < 6) ? workDays++ : false;
            currDate.setDate(currDate.getDate() + 1);

        }
        return workDays;
    }

    let workDaysToDL = getWorkDaysTo(deadline);

    if ((totalSkope / dailyProductivity) <= workDaysToDL) {
        console.log(`All tasks will be done in ${workDaysToDL - totalSkope / dailyProductivity} days before deadline!`)
    } else {
        console.log(`Team needs ${(totalSkope / dailyProductivity - workDaysToDL) * 8} additional hours to finish all tasks in time!`)
    }

}

productivityEval(teamProductivity, backLog, deadLine);





